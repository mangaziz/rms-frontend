import React from 'react'
import { connect } from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import Checkbox from 'material-ui/Checkbox'
import { formatDateWithShortMonthName } from '../../../helpers'
import { dispatchFetchEmployeeFamilies } from '../../../store/employee'

export class FamilyView extends React.Component {
  static propTypes = {
    employee: React.PropTypes.object,
    employees: React.PropTypes.object,
    dispatch: React.PropTypes.func
  }
  tableConfig;

  constructor (props) {
    super(props)

    // Table configuration
    this.tableConfig = {
      showRowHover: true,
      selectable: false,
      multiSelectable: false,
      enableSelectAll: false,
      deselectOnClickaway: false,
      showCheckboxes: false
    }

    this.state = {
      employeeIndex: null
    }
  }

  renderTableRow () {
    return this.props.employee.families.map((family, idx) => {
      return (
        <TableRow key={idx}>
          <TableRowColumn>{ family.name }</TableRowColumn>
          <TableRowColumn>{ family.gender }</TableRowColumn>
          <TableRowColumn>{ (family.birthDate !== null) ? formatDateWithShortMonthName(new Date(family.birthDate)) : '-' }</TableRowColumn>
          <TableRowColumn>{ family.type }</TableRowColumn>
          <TableRowColumn>
            <Checkbox
              checked={family.active}
            />
          </TableRowColumn>
          <TableRowColumn>
            <IconButton>
              <FontIcon className='material-icons'>mode_edit</FontIcon>
              <FontIcon className='material-icons'>delete</FontIcon>
            </IconButton>
          </TableRowColumn>
        </TableRow>
      )
    })
  }

  renderView () {
    return (
      <div>
        <Table
          fixedHeader={this.tableConfig.fixedHeader}
          fixedFooter={this.tableConfig.fixedFooter}
          selectable={this.tableConfig.selectable}
          multiSelectable={this.tableConfig.multiSelectable}
          >
          <TableHeader
            displaySelectAll={this.tableConfig.showCheckboxes}
            adjustForCheckbox={this.tableConfig.showCheckboxes}
            enableSelectAll={this.tableConfig.enableSelectAll}
            >
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Gender</TableHeaderColumn>
              <TableHeaderColumn>DOB</TableHeaderColumn>
              <TableHeaderColumn>Type</TableHeaderColumn>
              <TableHeaderColumn>Active</TableHeaderColumn>
              <TableHeaderColumn>&nbsp;</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={this.tableConfig.showCheckboxes}
            deselectOnClickaway={this.tableConfig.deselectOnClickaway}
            showRowHover={this.tableConfig.showRowHover}
            stripedRows={this.tableConfig.stripedRows}
            >
            { this.renderTableRow() }
          </TableBody>
        </Table>
      </div>
    )
  }

  // Fetching data from the API
  fetchFamilies () {
    // Is there any employee selected? Is the employee index changed?
    if (typeof this.props.employee !== 'undefined' &&
      this.state.employeeIndex !== this.props.employees.currentEmployee) {
      // Dispatching employments fetch
      dispatchFetchEmployeeFamilies(
        this.props.employees.currentEmployee,
        this.props.employee._links.employeeFamilies.href
      )(this.props.dispatch)

      // Change the state's employee index
      this.state.employeeIndex = this.props.employees.currentEmployee
    }
  }

  componentDidUpdate () {
    this.fetchFamilies()
  }

  componentDidMount () {
    this.fetchFamilies()
  }

  render () {
    if (typeof this.props.employee !== 'undefined') {
      if (typeof this.props.employee.families !== 'undefined') {
        return this.renderView()
      } else {
        return <p>There is no Family Member found for this employee</p>
      }
    } else {
      return <p>Please choose an Employee from the left sidebar</p>
    }
  }
}

// Mappers for connect()
const mapStateToProps = (state) => {
  return {
    employees: state.employee,
    employee: state.employee.modifiedEmployees[state.employee.currentEmployee]
  }
}

export default connect(mapStateToProps)(muiThemeable()(FamilyView))
