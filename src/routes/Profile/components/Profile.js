import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Row, Col } from 'react-grid-system'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import RaisedButton from 'material-ui/RaisedButton'
import DatePicker from 'material-ui/DatePicker'
import MenuItem from 'material-ui/MenuItem'
import Avatar from 'material-ui/Avatar'
import './Profile.scss'

const floatingLabelStyles = (palette) => {
  return {
    color: palette.lightBlack
  }
}

const selectFieldStyle = {
  fontSize: '16px'
}

const textFieldStyle = {
  fontSize: '16px !important'
}

class ProfileView extends Component {
  static propTypes = {
    muiTheme: PropTypes.object.isRequired,
    developmentStages: PropTypes.array,
    divisions: PropTypes.array,
    employee: PropTypes.object
  }

  constructor (props) {
    super(props)

    this.state = {}
  }

  getValue (column) {
    return (
      (typeof this.state === 'undefined' || typeof this.state[column] === 'undefined' || this.state[column] == null)
        ? ''
        : this.state[column]
    )
  }

  getDate (column) {
    const val = this.getValue(column)
    return (val === '') ? null : (new Date(val))
  }

  setValue (column, value) {
    this.setState({ [column]: value })
  }

  generateGrade () {
    return this.props.developmentStages.map((ds, idx) => {
      return (<MenuItem value={ds.id} primaryText={'DS' + ds.stage + ' - ' + ds.grade} key={idx} />)
    })
  }

  generateDivision () {
    return this.props.divisions.map((division, idx) => {
      return (<MenuItem value={division.id} primaryText={division.name} key={idx} />)
    })
  }

  renderView () {
    return (
      (
        <div className='profile-container'>
          <Row>
            <Col md={5}>
              <TextField
                floatingLabelText='First Name'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('firstName')}
                onChange={(event) => this.setValue('firstName', event.target.value)}
                inputStyle={textFieldStyle}
              />
            </Col>
            <Col md={5}>
              <TextField
                floatingLabelText='Sub Division'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('subDivision')}
                onChange={(event) => this.setValue('subDivision', event.target.value)}
                inputStyle={textFieldStyle}
              />
            </Col>
            <Col md={2}>
              <Avatar size={90} className='avatar-pics' src={this.getValue('avatar')} />
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <TextField
                floatingLabelText='Last Name'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('lastName')}
                onChange={(event) => this.setValue('lastName', event.target.value)}
                inputStyle={textFieldStyle}
              />
            </Col>
            <Col md={5}>
              <SelectField
                floatingLabelText='Status'
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                floatingLabelFixed
                selectedMenuItemStyle={selectFieldStyle}
                menuItemStyle={selectFieldStyle}
                labelStyle={selectFieldStyle}
                value={this.getValue('status')}
                onChange={(event, index, value) => this.setValue('status', value)}
              >
                <MenuItem value primaryText='Active' />
                <MenuItem value={false} primaryText='Inactive' />
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <SelectField
                floatingLabelText='Gender'
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                floatingLabelFixed
                selectedMenuItemStyle={selectFieldStyle}
                menuItemStyle={selectFieldStyle}
                labelStyle={selectFieldStyle}
                value={this.getValue('gender')}
                onChange={(event, index, value) => this.setValue('gender', value)}
              >
                <MenuItem value='Male' primaryText='Male' />
                <MenuItem value='Female' primaryText='Female' />
              </SelectField>
            </Col>
            <Col md={5}>
              <DatePicker
                floatingLabelText='Suspend Date'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                onChange={(event, date) => this.setValue('suspendDate', date)}
                value={this.getDate('suspendDate')}
              />
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <DatePicker
                floatingLabelText='Birth Date'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                onChange={(event, date) => this.setValue('birthDate', date)}
                value={this.getDate('birthDate')}
              />
            </Col>
            <Col md={5}>
              <DatePicker
                floatingLabelText='Hired Date'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                onChange={(event, date) => this.setValue('hiredDate', date)}
                value={this.getDate('hiredDate')}
              />
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <TextField
                floatingLabelText='Nationality'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('nationality')}
                onChange={(e) => this.setValue('nationality', e)}
                inputStyle={textFieldStyle}
              />
            </Col>
            <Col md={5}>
              <SelectField
                floatingLabelText='Grade'
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                floatingLabelFixed
                selectedMenuItemStyle={selectFieldStyle}
                menuItemStyle={selectFieldStyle}
                labelStyle={selectFieldStyle}
                value={this.getValue('grade')}
                onChange={(event, index, value) => this.setValue('grade', value)}
              >
                { this.generateGrade() }
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <SelectField
                floatingLabelText='Marital Status'
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                floatingLabelFixed
                selectedMenuItemStyle={selectFieldStyle}
                menuItemStyle={selectFieldStyle}
                labelStyle={selectFieldStyle}
                value={this.getValue('maritalStatus')}
                onChange={(event, index, value) => this.setValue('maritalStatus', value)}
              >
                <MenuItem value='Single' primaryText='Single' />
                <MenuItem value='Married' primaryText='Married' />
                <MenuItem value='Divorced' primaryText='Divorced' />
              </SelectField>
            </Col>
            <Col md={5}>
              <SelectField
                floatingLabelText='Division'
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                floatingLabelFixed
                selectedMenuItemStyle={selectFieldStyle}
                menuItemStyle={selectFieldStyle}
                labelStyle={selectFieldStyle}
                value={this.getValue('division')}
                onChange={(event, index, value) => this.setValue('division', value)}
              >
                { this.generateDivision() }
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <TextField
                floatingLabelText='Phone'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('phone')}
                onChange={(e) => this.setValue('phone', e)}
                inputStyle={textFieldStyle}
              />
            </Col>
            <Col md={5}>
              <TextField
                floatingLabelText='Email'
                floatingLabelFixed
                floatingLabelStyle={floatingLabelStyles(this.props.muiTheme.palette)}
                value={this.getValue('email')}
                onChange={(e) => this.setValue('email', e)}
                inputStyle={textFieldStyle}
              />
            </Col>
          </Row>
          <div className='form-footer' style={{ backgroundColor: this.props.muiTheme.palette.primary2Color }}>
            <RaisedButton label='Cancel' />
            <RaisedButton label='Save' secondary />
          </div>
        </div>
      )
    )
  }

  render () {
    // Is there any selected employee?
    if (typeof this.props.employee !== 'undefined') {
      // Only change the state IF the ID is CHANGE
      if (this.state.id !== this.props.employee.id) {
        this.state = this.props.employee
      }

      return this.renderView()
    } else {
      return <p>Please choose an Employee from the left sidebar</p>
    }
  }
}

export default connect((state) => {
  return {
    employee: state.employee.employees[state.employee.currentEmployee],
    developmentStages: state.developmentStage,
    divisions: state.division
  }
})(muiThemeable()(ProfileView))
