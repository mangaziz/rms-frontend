import React from 'react'
import { connect } from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import { formatDateWithShortMonthName } from '../../../helpers'

export class GradeView extends React.Component {
  static propTypes = {
    employee: React.PropTypes.object
  }
  tableConfig;

  constructor (props) {
    super(props)

    // Table configuration
    this.tableConfig = {
      showRowHover: true,
      selectable: false,
      multiSelectable: false,
      enableSelectAll: false,
      deselectOnClickaway: false,
      showCheckboxes: false
    }
  }

  renderTableRow () {
    return this.props.employee.developmentStages.map((ds, idx) => {
      return (
        <TableRow key={idx}>
          <TableRowColumn>{ 'DS' + ds.stage }</TableRowColumn>
          <TableRowColumn>{ ds.grade }</TableRowColumn>
          <TableRowColumn>{ formatDateWithShortMonthName(new Date(ds.startDate)) }</TableRowColumn>
          <TableRowColumn>{
            (ds.endDate != null) ? formatDateWithShortMonthName(new Date(ds.endDate)) : 'Present'
          }</TableRowColumn>
          <TableRowColumn>
            <IconButton>
              <FontIcon className='material-icons'>delete</FontIcon>
            </IconButton>
          </TableRowColumn>
        </TableRow>
      )
    })
  }

  renderView () {
    return (
      <div>
        <Table
          fixedHeader={this.tableConfig.fixedHeader}
          fixedFooter={this.tableConfig.fixedFooter}
          selectable={this.tableConfig.selectable}
          multiSelectable={this.tableConfig.multiSelectable}
          >
          <TableHeader
            displaySelectAll={this.tableConfig.showCheckboxes}
            adjustForCheckbox={this.tableConfig.showCheckboxes}
            enableSelectAll={this.tableConfig.enableSelectAll}
            >
            <TableRow>
              <TableHeaderColumn>DS</TableHeaderColumn>
              <TableHeaderColumn>Grade</TableHeaderColumn>
              <TableHeaderColumn>Start Date</TableHeaderColumn>
              <TableHeaderColumn>End Date</TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={this.tableConfig.showCheckboxes}
            deselectOnClickaway={this.tableConfig.deselectOnClickaway}
            showRowHover={this.tableConfig.showRowHover}
            stripedRows={this.tableConfig.stripedRows}
            >
            { this.renderTableRow() }
          </TableBody>
        </Table>
      </div>
    )
  }

  render () {
    if (typeof this.props.employee !== 'undefined') {
      if (typeof this.props.employee.developmentStages !== 'undefined') {
        return this.renderView()
      } else {
        return <p>There is no Grade history found for this employee</p>
      }
    } else {
      return <p>Please choose an Employee from the left sidebar</p>
    }
  }
}

// Mappers for connect()
const mapStateToProps = (state) => {
  return {
    employee: state.employee.modifiedEmployees[state.employee.currentEmployee]
  }
}

export default connect(mapStateToProps)(muiThemeable()(GradeView))
