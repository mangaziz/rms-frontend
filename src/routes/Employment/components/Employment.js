import React from 'react'
import { connect } from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Container, Row, Col } from 'react-grid-system'
import './Employment.scss'
import TextField from 'material-ui/TextField'
import FontIcon from 'material-ui/FontIcon'
import { dispatchFetchEmployeeEmployments } from '../../../store/employee'
import { formatDateWithFullMonthName } from '../../../helpers'

export class EmploymentView extends React.Component {
  static propTypes = {
    employees: React.PropTypes.object,
    employee: React.PropTypes.object,
    dispatch: React.PropTypes.func
  }

  mainTimeTextFieldStyle = {
    fontWeight: 'bolder'
  }

  textFieldStyle = {
    month: {
      ...this.mainTimeTextFieldStyle,
      fontSize: '9px',
      height: '15px',
      lineHeight: '0px'
    },
    year: {
      ...this.mainTimeTextFieldStyle,
      fontSize: '20px',
      height: '30px',
      lineHeight: '6px'
    },
    hint: {
      color: '#D6D6D6'
    },
    jobDescs: {
      height: '22px',
      lineHeight: '0px',
      fontSize: '12px',
      width: '100%'
    },
    jobDescsHintStyle: {
      bottom: '10px'
    },
    startWidth: {
      width: '50px'
    },
    endWidth: {
      width: '70px'
    }
  }

  constructor (props) {
    super(props)

    this.state = {
      employeeIndex: null,
      employments: {}
    }
  }

  // Fetching data from the API
  fetchEmployments () {
    // Is there any employee selected? Is the employee index changed?
    if (typeof this.props.employee !== 'undefined' &&
      this.state.employeeIndex !== this.props.employees.currentEmployee) {
      // Dispatching employments fetch
      dispatchFetchEmployeeEmployments(
        this.props.employees.currentEmployee,
        this.props.employee._links.employeeEmployments.href
      )(this.props.dispatch)

      // Change the state's employee index
      this.state.employeeIndex = this.props.employees.currentEmployee
    }
  }

  componentDidUpdate () {
    this.fetchEmployments()
  }

  componentDidMount () {
    this.fetchEmployments()
  }

  // Generate Employment Descs
  generateJobDescs (employmentIndex) {
    let jobDescriptions = []
    let lastKey = 0

    // Is employment descriptions exists?
    if (typeof this.props.employee.employments[employmentIndex].employmentDescs !== 'undefined') {
      const descs = this.props.employee.employments[employmentIndex].employmentDescs.map((desc, descIdx) => {
        lastKey++
        return (
          <li key={descIdx}>
            <FontIcon className='material-icons'>radio_button_checked</FontIcon>
            <TextField underlineStyle={{ border: 'none' }}
              className='job-descs-field'
              hintStyle={this.textFieldStyle.jobDescsHintStyle}
              style={this.textFieldStyle.jobDescs}
              value={desc.description}
              name={'jobDesc-' + descIdx}
              />
          </li>
        )
      })

      jobDescriptions.push(descs)
    }

    // Push default element (add new description)
    jobDescriptions.push(
      <li key={lastKey}>
        <FontIcon className='material-icons'>radio_button_checked</FontIcon>
        <TextField hintText='Add new here'
          underlineStyle={{ border: 'none' }}
          className='job-descs-field'
          hintStyle={this.textFieldStyle.jobDescsHintStyle}
          style={this.textFieldStyle.jobDescs}
          name='addNew'
          />
      </li>
    )

    return jobDescriptions
  }

  // Employee generator
  generateEmployments () {
    // Iterate and return the components
    return this.props.employee.employments.map((employment, employmentIdx) => {
      // Initialize main variables
      const splitDateFormat = /(\d+)\s(\w+)\s(\d+)/
      const dates = {
        startDate: splitDateFormat.exec(formatDateWithFullMonthName(new Date(employment.startDate))),
        endDate: (
          (employment.endDate !== null)
          ? splitDateFormat.exec(formatDateWithFullMonthName(new Date(employment.endDate)))
          : null
        )
      }

      // Set employments data to state
      this.state.employments = Object.assign({}, employment, {
        startMonth: dates.startDate[2].toUpperCase(),
        startYear: dates.startDate[3].toUpperCase(),
        endMonth: (dates.endDate !== null) ? dates.endDate[2].toUpperCase() : null,
        endYear: (dates.endDate !== null) ? dates.endDate[3].toUpperCase() : null
      })

      return (
        <Row className='employment' key={employmentIdx}>
          <Col className='left-pane' md={3}>
            <table width='100%' cellSpacing='0' cellPadding='0'>
              <tbody>
                <tr>
                  <td>
                    <TextField hintText='Start Month'
                      hintStyle={this.textFieldStyle.hint}
                      style={{ ...this.textFieldStyle.month, width: '45px' }}
                      underlineStyle={{ border: 'none' }}
                      name='startMonth'
                      value={this.state.employments.startMonth} />
                  </td>
                  <td className='time-separator'>{ (this.state.employments.endMonth === null) ? '' : '-' }</td>
                  <td>
                    <TextField hintText='End Month'
                      hintStyle={this.textFieldStyle.hint}
                      style={{ ...this.textFieldStyle.month, width: '95px' }}
                      underlineStyle={{ border: 'none' }}
                      name='endMonth'
                      value={(this.state.employments.endMonth === null) ? ' ' : this.state.employments.endMonth} />
                  </td>
                </tr>
                <tr>
                  <td>
                    <TextField hintText='Start Year'
                      hintStyle={this.textFieldStyle.hint}
                      style={{ ...this.textFieldStyle.year, width: '45px' }}
                      underlineStyle={{ border: 'none' }}
                      name='startYear'
                      value={this.state.employments.startYear} />
                  </td>
                  <td className='year-separator time-separator'>-</td>
                  <td>
                    <TextField hintText='End Year'
                      hintStyle={this.textFieldStyle.hint}
                      style={{ ...this.textFieldStyle.year, width: '95px' }}
                      underlineStyle={{ border: 'none' }}
                      name='endYear'
                      value={(this.state.employments.endYear === null) ? 'PRESENT' : this.state.employments.endYear} />
                  </td>
                </tr>
              </tbody>
            </table>
            <hr className='divider' />
            <TextField className='company-name below-divider'
              hintStyle={this.textFieldStyle.hint}
              underlineStyle={{ border: 'none' }}
              name='companyName'
              value={this.state.employments.company.toUpperCase()} />
            <TextField className='position-name below-divider'
              hintStyle={this.textFieldStyle.hint}
              underlineStyle={{ border: 'none' }}
              name='positionName'
              value={this.state.employments.position.toUpperCase()} />
          </Col>
          <Col className='right-pane' md={9}>
            <span className='title'>JOB DESCRIPTION</span>
            <ul className='job-descs'>
              {this.generateJobDescs(employmentIdx)}
            </ul>
          </Col>
        </Row>
      )
    })
  }

  // Rendering employment
  render () {
    if (typeof this.props.employee === 'undefined') {
      return <p>Please choose an Employee from the left sidebar</p>
    } else if (typeof this.props.employee.employments === 'undefined') {
      return <p>Trying to fetch employee's employment(s)</p>
    } else if (this.props.employee.employments.length === 0) {
      return <p>There is no Employment history found for this employee</p>
    } else {
      return (
        <Container className='employment-container'>
          { this.generateEmployments() }
        </Container>
      )
    }
  }
}

export default connect(state => {
  return {
    employees: state.employee,
    employee: state.employee.modifiedEmployees[state.employee.currentEmployee]
  }
})(muiThemeable()(EmploymentView))
