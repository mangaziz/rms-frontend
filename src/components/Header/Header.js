import React from 'react'
import { connect } from 'react-redux'
import './Header.scss'
import muiThemeable from 'material-ui/styles/muiThemeable'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import AppBar from 'material-ui/AppBar'

// Header Right
const iconStyles = {
  marginRight: 24,
  color: '#fff'
}

export class Header extends React.Component {
  static propTypes = {
    muiTheme: React.PropTypes.object
    // employee: React.PropTypes.object
  }

  generateHeaderLeft () {
    return (
      <div className='appbar-profile'>
        <img src='http://www.material-ui.com/images/ok-128.jpg' />
        <div className='appbar-profile-text'>
          <span className='appbar-profile-text-username'>Anggie Aziz</span>
          <span className='appbar-profile-text-position'>SE-PG</span>
        </div>
      </div>
    )
  }

  generateHeaderRight () {
    return (
      <div style={{ marginTop: '-8px' }}>
        <IconButton tooltip='Settings'>
          <FontIcon className='material-icons' style={iconStyles} color={iconStyles.color}>settings</FontIcon>
        </IconButton>
        <IconButton tooltip='Logout'>
          <FontIcon className='material-icons' style={iconStyles} color={iconStyles.color}>power_settings_new</FontIcon>
        </IconButton>
      </div>
    )
  }

  render () {
    return (
      <AppBar
        style={{ backgroundColor: this.props.muiTheme.palette.primary1Color }}
        title={this.generateHeaderLeft()}
        iconElementRight={this.generateHeaderRight()}
        className='header' />
    )
  }
}

export default connect((state) => {
  return {
    employee: state.employee.modifiedEmployees[state.employee.currentEmployee]
  }
})(muiThemeable()(Header))
