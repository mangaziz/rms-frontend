import React from 'react'
import { IndexLink, Link } from 'react-router'
import './Content.scss'
import muiThemeable from 'material-ui/styles/muiThemeable'
import ContentTab from './ContentTab'
import { Container } from 'react-grid-system'

export class Content extends React.Component {
  static propTypes = {
    // muiThemeable: React.PropTypes.object.isRequere,
    currentRoute: React.PropTypes.string,
    children: React.PropTypes.object.isRequired
  }
  render () {
    return (
      <Container>
        <ContentTab currentRoute={this.props.currentRoute} className='content-tab' />
        <Container style={{ position: 'relative' }}>
          {this.props.children}
        </Container>
      </Container>
    )
  }
}

export default muiThemeable()(Content)
