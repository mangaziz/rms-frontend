import React from 'react'
import { connect } from 'react-redux'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Link } from 'react-router'
import './ContentTab.scss'
import FontIcon from 'material-ui/FontIcon'
import Layer from 'material-ui/svg-icons/maps/layers'
import WC from 'material-ui/svg-icons/notification/wc'
import { Tabs, Tab } from 'material-ui/Tabs'
import { Row, Col } from 'react-grid-system'

const tabStyle = (secondPrimaryColor) => {
  return {
    backgroundColor: secondPrimaryColor
  }
}

export class ContentTab extends React.Component {
  static propTypes = {
    muiTheme: React.PropTypes.object.isRequired,
    currentRoute: React.PropTypes.string
  }
  tabRoutes = [
    {
      icon: <FontIcon className='fa fa-user' />,
      to: '/profile'
    },
    {
      icon: <FontIcon className='fa fa-history' />,
      to: '/employment'
    },
    {
      icon: <Layer />,
      to: '/grade'
    },
    {
      icon: <WC />,
      to: '/family'
    },
    {
      icon: <FontIcon className='fa fa-home' />,
      to: '/address'
    },
    {
      icon: <FontIcon className='fa fa-map-marker' />,
      to: '/location'
    }
  ]

  findRoutesIndex () {
    let currentRouteIndex = 0
    this.tabRoutes.map((tabRoute, idx) => {
      if (tabRoute.to === this.props.currentRoute) {
        currentRouteIndex = idx
      }
    })

    return currentRouteIndex
  }

  render () {
    return (
      <Row style={tabStyle(this.props.muiTheme.palette.primary2Color)}>
        <Col xs={12}>
          <Tabs tabItemContainerStyle={tabStyle(this.props.muiTheme.palette.primary2Color)}
            initialSelectedIndex={this.findRoutesIndex()}>
            {
              this.tabRoutes.map((tabRoute, index) => {
                return <Tab icon={tabRoute.icon} containerElement={<Link to={tabRoute.to} />} key={index} />
              })
            }
          </Tabs>
        </Col>
      </Row>
    )
  }
}

export default connect((state) => {
  return {}
})(muiThemeable()(ContentTab))
