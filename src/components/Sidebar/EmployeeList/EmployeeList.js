import React from 'react'
import { connect } from 'react-redux'
import './EmployeeList.scss'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { List, ListItem } from 'material-ui/List'
import FontIcon from 'material-ui/FontIcon'
import Avatar from 'material-ui/Avatar'
import Divider from 'material-ui/Divider'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import { dispatchSetActiveEmployee } from '../../../store/employee'

class EmployeeList extends React.Component {
  static propTypes = {
    muiTheme: React.PropTypes.object.isRequired,
    employee: React.PropTypes.object
  }

  constructor (props) {
    super(props)

    // Lets bind something
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      employees: []
    }
  }

  handleClick (index) {
    dispatchSetActiveEmployee(this.props, index)
  }

  renderItems () {
    return this.props.employee.modifiedEmployees.map((employee, idx) => {
      return (
        <div key={idx} onClick={() => { this.handleClick(idx) }}>
          <ListItem
            primaryText={employee.fullName}
            leftAvatar={<Avatar size={64} className='employee-list-avatar' src={employee.avatar} />}
            secondaryText={
              <p className='employee-list-secondary'>
                <span>{ employee.grade + (
                  (employee.division != null) ? ', ' + employee.division : ''
                ) + (
                  (employee.subDivision != null) ? ', ' + employee.subDivision : ''
                ) }</span><br />
                <span>{ employee.location + ', ' + employee.phone }</span>
              </p>
            }
            secondaryTextLines={2}
            rightIcon={
              <div className='employee-list-right'>
                <span>{ employee.hiredDate }</span>
                <div className='employee-list-right-icons'>
                  <FontIcon className='material-icons employee-list-right-icons-radio'
                    color={this.props.muiTheme.palette.primary1Color}>radio_button_checked</FontIcon>
                  <FontIcon className='material-icons employee-list-right-icons-chevron'
                    color={this.props.muiTheme.palette.lightGrey}>chevron_right</FontIcon>
                </div>
              </div>
            }
          />
          <Divider />
        </div>
      )
    })
  }

  render () {
    return (
      <div className='employee-list'>
        <List>
          {this.renderItems()}
        </List>
        <FloatingActionButton secondary className='employee-list-float'>
          <ContentAdd />
        </FloatingActionButton>
      </div>
    )
  }
}

export default connect((state) => {
  return { employee: state.employee }
})(muiThemeable()(EmployeeList))
