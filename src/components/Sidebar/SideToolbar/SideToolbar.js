import React from 'react'
import './SideToolbar.scss'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import FontIcon from 'material-ui/FontIcon'
import IconButton from 'material-ui/IconButton'
import Chip from 'material-ui/Chip'
import TextField from 'material-ui/TextField'
import { Row, Col } from 'react-grid-system'

const textFieldTextStyle = (fontColor) => {
  return {
    color: fontColor,
    fontSize: '14px'
  }
}

export class SideToolbar extends React.Component {
  static propTypes = {
    muiTheme: React.PropTypes.object.isRequired
  }
  render () {
    return (
      <Row>
        <Toolbar className='sidebar-content-toolbar'
          style={{ backgroundColor: this.props.muiTheme.palette.primary2Color }}>
          <ToolbarGroup>
            <Col xs={1} md={1} sm={1}>
              <FontIcon className='material-icons' color={this.props.muiTheme.palette.white}>search</FontIcon>
            </Col>
            <Col xs={7} md={5} sm={5}>
              <TextField className='search-textfield'
                hintText='Search'
                underlineShow={false}
                hintStyle={textFieldTextStyle(this.props.muiTheme.palette.white)}
                inputStyle={textFieldTextStyle(this.props.muiTheme.palette.white)} />
            </Col>
            <Col xs={4} md={6} sm={6} style={{ paddingRight: 0, textAlign: 'right' }}>
              <IconButton tooltip='Sort' className='right-button'>
                <FontIcon className='fa fa-sort-amount-desc' color={this.props.muiTheme.palette.white} />
              </IconButton>
              <IconButton tooltip='Filter' className='right-button'>
                <FontIcon className='fa fa-filter' color={this.props.muiTheme.palette.white} />
              </IconButton>
              <Chip className='chip'>9999</Chip>
            </Col>
          </ToolbarGroup>
        </Toolbar>
      </Row>
    )
  }
}

export default muiThemeable()(SideToolbar)
