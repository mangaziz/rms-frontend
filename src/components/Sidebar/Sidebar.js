import React from 'react'
import './Sidebar.scss'
import muiThemeable from 'material-ui/styles/muiThemeable'
import { Container } from 'react-grid-system'
import SideToolbar from './SideToolbar'
import EmployeeList from './EmployeeList'

export const Sidebar = (props) => (
  <div className='sidebar-content'>
    <Container fluid>
      <SideToolbar />
      <EmployeeList />
    </Container>
  </div>
)

export default muiThemeable()(Sidebar)
