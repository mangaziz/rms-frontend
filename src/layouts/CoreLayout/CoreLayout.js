import React from 'react'
import Header from '../../components/Header'
import Sidebar from '../../components/Sidebar'
import Content from '../../components/Content'
import './CoreLayout.scss'
import '../../styles/core.scss'
import '../../styles/font-awesome/scss/font-awesome.scss'
import { Container, Row, Col } from 'react-grid-system'

export class CoreLayout extends React.Component {
  static propTypes = {
    location: React.PropTypes.object
  }

  render () {
    return (
      <div className='container text-center'>
        <Container fluid>
          <Row>
            <Col xs={12}>
              <Header />
            </Col>
          </Row>
          <div className='core-layout__viewport'>
            <Row>
              <Col xs={4} className='sidebar-container' style={{ paddingRight: 0 }}>
                <Sidebar />
              </Col>
              <Col xs={8} style={{ paddingLeft: 0 }}>
                <Content currentRoute={this.props.location.pathname}>{this.props.children}</Content>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default CoreLayout
