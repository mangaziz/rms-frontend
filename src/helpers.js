export const API_URL = 'http://localhost:8080/'

export const removeKey = (obj, key) => {
  let { [key]: omit, ...res } = obj
  return res
}

// Month names
const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const fullMonthNames = [
  'January', 'February', 'March', 'April',
  'May', 'June', 'July', 'August',
  'September', 'October', 'November', 'December'
]

// Main date formatter
const formatDate = (date, kind) => {
  return date.getDate() +
          ' ' +
          ((kind === 'full') ? fullMonthNames[date.getMonth()] : monthNames[date.getMonth()]) +
          ' ' +
          date.getFullYear()
}

export const formatDateWithShortMonthName = (date) => {
  return formatDate(date, 'short')
}

export const formatDateWithFullMonthName = (date) => {
  return formatDate(date, 'full')
}
