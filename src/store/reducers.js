import { combineReducers } from 'redux'
import locationReducer from './location'
import employeeReducer from './employee'
import developmentStageReducer from './developmentStage'
import divisionReducer from './division'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    employee: employeeReducer,
    developmentStage: developmentStageReducer,
    division: divisionReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
