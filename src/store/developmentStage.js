import { API_URL } from '../helpers'
import axios from 'axios'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_DEVELOPMENT_STAGE = 'FETCH_DEVELOPMENT_STAGE'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchDevelopmentStage (developmentStages) {
  return {
    type    : FETCH_DEVELOPMENT_STAGE,
    payload : developmentStages
  }
}

// ------------------------------------
// Specialized Action Creator (Thunk)
// ------------------------------------
export const dispatchFetchDevelopmentStage = ({ dispatch }) => {
  axios.get(API_URL + 'developmentStages').then((response) => {
    dispatch(fetchDevelopmentStage(response.data._embedded.developmentStages))
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = []
export default function developmentStageReducer (state = initialState, action) {
  switch (action.type) {
    case FETCH_DEVELOPMENT_STAGE: {
      return state.concat(action.payload)
    }
    default: {
      return state
    }
  }
}
