import { API_URL, removeKey, formatDateWithShortMonthName } from '../helpers'
import axios from 'axios'

// ------------------------------------
// Constants
// ------------------------------------
export const CREATE_EMPLOYEE = 'CREATE_EMPLOYEE'
export const UPDATE_ORIGINAL_EMPLOYEE = 'UPDATE_ORIGINAL_EMPLOYEE'
export const UPDATE_MODIFIED_EMPLOYEE = 'UPDATE_MODIFIED_EMPLOYEE'
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE'
export const FETCH_EMPLOYEES = 'FETCH_EMPLOYEES'
export const SET_ACTIVE_EMPLOYEE = 'SET_ACTIVE_EMPLOYEE'
export const FETCH_EMPLOYEE_EMPLOYMENTS = 'FETCH_EMPLOYEE_EMPLOYMENTS'
export const FETCH_EMPLOYEE_EMPLOYMENT_DESCS = 'FETCH_EMPLOYEE_EMPLOYMENT_DESCS'
export const FETCH_EMPLOYEE_FAMILIES = 'FETCH_EMPLOYEE_FAMILIES'

// ------------------------------------
// Actions
// ------------------------------------
export function createEmployee (employee) {
  return {
    type    : CREATE_EMPLOYEE,
    payload : employee
  }
}
function updateEmployee (type, employeeIndex, changedEmployeeData) {
  return {
    type    : type,
    payload : {
      employeeIndex: employeeIndex,
      changedEmployeeData: changedEmployeeData
    }
  }
}
export function updateOriginalEmployee (employeeIndex, changedEmployeeData) {
  return updateEmployee(UPDATE_ORIGINAL_EMPLOYEE, employeeIndex, changedEmployeeData)
}
export function updateModifiedEmployee (employeeIndex, changedEmployeeData) {
  return updateEmployee(UPDATE_MODIFIED_EMPLOYEE, employeeIndex, changedEmployeeData)
}
export function deleteEmployee (employeeID) {
  return {
    type    : DELETE_EMPLOYEE,
    payload : employeeID
  }
}
export function fetchEmployees (employees) {
  return {
    type    : FETCH_EMPLOYEES,
    payload : employees
  }
}
export function setActiveEmployee (employeeIndex) {
  return {
    type    : SET_ACTIVE_EMPLOYEE,
    payload : employeeIndex
  }
}
export function fetchEmployeeEmploymentDescs (employeeIndex, employmentIndex, employmentDescs) {
  return {
    type    : FETCH_EMPLOYEE_EMPLOYMENT_DESCS,
    payload : {
      employeeIndex: employeeIndex,
      employmentIndex: employmentIndex,
      employmentDescs: employmentDescs
    }
  }
}

// ------------------------------------
// Specialized Action Creator (Thunk)
// ------------------------------------
export const dispatchCreateEmployee = ({ dispatch }) => {
  return (employee) => dispatch(createEmployee(employee))
}
export const dispatchUpdateEmployee = ({ dispatch }) => {
  return (changedEmployeeData) => dispatch(updateEmployee(changedEmployeeData))
}
export const dispatchDeleteEmployee = ({ dispatch }) => {
  return (employeeID) => dispatch(deleteEmployee(employeeID))
}
export const dispatchFetchEmployees = () => {
  return ({ dispatch, getState }) => {
    // Getting employees and dispatch it
    axios.get(API_URL + 'employees').then((response) => {
      dispatch(fetchEmployees(response.data._embedded.employees))

      // Storing modified employees state in variable and iterate it
      const state = getState().employee.modifiedEmployees
      state.map((employee, employeeIdx) => {
        // Getting Development Stages of an employee
        axios.get(employee._links.employeeDevelopmentStages.href).then((edsResponse) => {
          let developmentStagesPayload = edsResponse.data._embedded.employeeDevelopmentStages.map((ds) => {
            return {
              id: ds.id,
              startDate: ds.startDate,
              endDate: ds.endDate
            }
          })

          // Getting it's stage and grade label and do dispatch
          edsResponse.data._embedded.employeeDevelopmentStages.map((ds, dsIdx) => {
            axios.get(ds._links.developmentStage.href).then((dsResponse) => {
              developmentStagesPayload[dsIdx] = Object.assign({}, developmentStagesPayload[dsIdx], {
                developmentStageId: dsResponse.data.id,
                grade: dsResponse.data.grade,
                stage: dsResponse.data.stage
              })

              // Update modifiedEmployees' grade with his/her last grade
              if (dsIdx === 0) {
                dispatch(updateModifiedEmployee(employeeIdx, { grade: dsResponse.data.grade }))
              }
            })
          })

          // Append developmentStages to modifiedEmployees' data
          dispatch(updateModifiedEmployee(employeeIdx, { developmentStages: developmentStagesPayload }))
        })
      })
    })
  }
}
export const dispatchSetActiveEmployee = ({ dispatch }, employeeIndex) => {
  dispatch(setActiveEmployee(employeeIndex))
}

// Addon fetchers
export const dispatchFetchEmployeeEmployments = (employeeIndex, employmentsURL) => {
  return (dispatch) => {
    axios.get(employmentsURL).then((employmentsResponse) => {
      const employments = employmentsResponse.data._embedded.employeeEmployments

      // Dispatch employments to modifiedEmployees
      dispatch(updateModifiedEmployee(employeeIndex, {
        employments: employments
      }))

      // If there is any employments, try to generate employment descriptions
      if (employments.length > 0) {
        employments.map((employment, employmentIdx) => {
          axios.get(employment._links.employeeEmploymentDescs.href).then((employmentDescsResponse) => {
            // Dispatch append descriptions to modifiedEmployees' employment
            dispatch(fetchEmployeeEmploymentDescs(
              employeeIndex,
              employmentIdx,
              employmentDescsResponse.data._embedded.employeeEmploymentDescs)
            )
          })
        })
      }
    })
  }
}

export const dispatchFetchEmployeeFamilies = (employeeIndex, familiesURL) => {
  return (dispatch) => {
    axios.get(familiesURL).then((familiesResponse) => {
      dispatch(updateModifiedEmployee(employeeIndex, { families: familiesResponse.data._embedded.employeeFamilies }))
    })
  }
}

// ------------------------------------
// Methods
// ------------------------------------
function modifyEmployeeData (employee) {
  // For easier data formatting
  const hiredDate = new Date(employee.hiredDate)

  return {
    id: employee.id,
    avatar: employee.avatar,
    fullName: employee.firstName + ' ' + employee.lastName,
    grade: 'N/A',
    division: 'CDC',
    subDivision: employee.subDivision,
    location: 'Bali',
    phone: employee.phone,
    hiredDate: formatDateWithShortMonthName(hiredDate),
    _links: employee._links
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  currentEmployee: null,
  employees: [],
  modifiedEmployees: []
}
export default function employeeReducer (state = initialState, action) {
  switch (action.type) {
    case FETCH_EMPLOYEES: {
      return {
        ...initialState,
        employees: action.payload,
        modifiedEmployees: action.payload.map((employee) => {
          return modifyEmployeeData(employee)
        })
      }
    }
    case CREATE_EMPLOYEE: {
      return {
        ...state,
        employees: state.employees.concat(action.payload),
        modifiedEmployees: state.modifiedEmployees.concat(action.payload.map((employee) => {
          return modifyEmployeeData(employee)
        }))
      }
    }
    case UPDATE_ORIGINAL_EMPLOYEE: {
      // Clone element to update
      const modifiedState = Object.assign({}, state)
      modifiedState.employees = Object.assign([], modifiedState.employees)

      // Update the employee data
      modifiedState.employees[action.payload.employeeIndex] = Object.assign(
        {},
        modifiedState.employees[action.payload.employeeIndex],
        action.payload.changedEmployeeData
      )

      return modifiedState
    }
    case UPDATE_MODIFIED_EMPLOYEE: {
      // Clone element to update
      const modifiedState = Object.assign({}, state)
      modifiedState.modifiedEmployees = Object.assign([], modifiedState.modifiedEmployees)

      // Update the employee data
      modifiedState.modifiedEmployees[action.payload.employeeIndex] = Object.assign(
        {},
        modifiedState.modifiedEmployees[action.payload.employeeIndex],
        action.payload.changedEmployeeData
      )

      return modifiedState
    }
    case DELETE_EMPLOYEE: {
      return removeKey(state.employees, action.payload)
    }
    case SET_ACTIVE_EMPLOYEE: {
      return { ...state, currentEmployee: action.payload }
    }
    case FETCH_EMPLOYEE_EMPLOYMENT_DESCS : {
      // Clone element to update
      const modifiedState = Object.assign({}, state)
      modifiedState.modifiedEmployees = Object.assign([], modifiedState.modifiedEmployees)

      // Breaking the current employee's reference by cloning it
      modifiedState.modifiedEmployees[action.payload.employeeIndex] = Object.assign(
        {},
        modifiedState.modifiedEmployees[action.payload.employeeIndex]
      )

      // Breaking the employments reference by cloning it
      modifiedState.modifiedEmployees[action.payload.employeeIndex].employments = Object.assign(
        [],
        modifiedState.modifiedEmployees[action.payload.employeeIndex].employments
      )

      // Updating the modified state with new employment description
      modifiedState.modifiedEmployees[action.payload.employeeIndex].employments[action.payload.employmentIndex] = Object.assign(
        {},
        modifiedState.modifiedEmployees[action.payload.employeeIndex].employments[action.payload.employmentIndex],
        { employmentDescs: action.payload.employmentDescs }
      )

      return modifiedState
    }
    default: {
      return state
    }
  }
}
