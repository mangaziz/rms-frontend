import { API_URL } from '../helpers'
import axios from 'axios'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_DIVISION = 'FETCH_DIVISION'

// ------------------------------------
// Actions
// ------------------------------------
export function fetchDivision (divisions) {
  return {
    type    : FETCH_DIVISION,
    payload : divisions
  }
}

// ------------------------------------
// Specialized Action Creator (Thunk)
// ------------------------------------
export const dispatchFetchDivision = ({ dispatch }) => {
  axios.get(API_URL + 'divisions').then((response) => {
    dispatch(fetchDivision(response.data._embedded.divisions))
  })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = []
export default function divisionReducer (state = initialState, action) {
  switch (action.type) {
    case FETCH_DIVISION: {
      return state.concat(action.payload)
    }
    default: {
      return state
    }
  }
}
