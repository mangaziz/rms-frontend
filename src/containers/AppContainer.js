import React, { Component, PropTypes } from 'react'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import { dispatchFetchEmployees } from '../store/employee'
import { dispatchFetchDevelopmentStage } from '../store/developmentStage'
import { dispatchFetchDivision } from '../store/division'

class AppContainer extends Component {
  static propTypes = {
    routes : PropTypes.object.isRequired,
    store  : PropTypes.object.isRequired
  }

  constructor (props) {
    super(props)

    // Initializing employee list when empty
    if (props.store.getState().employee.employees.length === 0) {
      dispatchFetchEmployees()(props.store)
    }

    // Get development stages data if empty
    if (props.store.getState().developmentStage.length === 0) {
      dispatchFetchDevelopmentStage(props.store)
    }

    // Get divisions data if empty
    if (props.store.getState().division.length === 0) {
      dispatchFetchDivision(props.store)
    }
  }

  shouldComponentUpdate () {
    return false
  }

  render () {
    const { routes, store } = this.props

    return (
      <Provider store={store}>
        <Router history={browserHistory} children={routes} />
      </Provider>
    )
  }
}

export default AppContainer
